﻿using ExchangeRates.DataBase;
using ExchangeRates.Models.XmlDocuments;
using System;
using System.Linq;
using System.Net.Http;
using System.Xml;

namespace ExchangeRates.Classes
{
    public class ExchangeRate
    {
        HttpClient httpClient = new HttpClient();
        XmlConverter xmlHelper = new XmlConverter();

        public async void Get(object obj)
        {
            var valCurs = new ValCurs();
            var currencyRateXml = new XmlDocument();
            var dbContext = new DataBaseContext();

            var response = await httpClient.GetAsync("http://www.cbr.ru/scripts/XML_daily.asp");
            if (response.StatusCode.GetHashCode() == 200)
            {
                currencyRateXml.LoadXml(await response.Content.ReadAsStringAsync());
                valCurs = xmlHelper.DeSerializeXmlToObject<ValCurs>(currencyRateXml);
            }

            var date = DateTime.Parse(valCurs.Date);

            if (dbContext.CurrencyRate.FirstOrDefault(x => x.Date == date) == null)
            {
                var currencyRate = valCurs.Valute.Select(x => new CurrencyRate
                { Date = date, Currency = x.CharCode, Value = double.Parse(x.Value) }).ToList();

                dbContext.CurrencyRate.AddRange(currencyRate);
                dbContext.SaveChanges();
            }
        }
    }
}