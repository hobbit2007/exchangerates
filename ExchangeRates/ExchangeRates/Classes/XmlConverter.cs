﻿using System.Xml;
using System.Xml.Serialization;

namespace ExchangeRates.Classes
{
    public class XmlConverter
    {
        /// <summary>
        /// Создание объекта(класса) из xml документа
        /// </summary>
        /// <typeparam name="T">Класс который используется для описания данных</typeparam>
        /// <param name="inputData">Входные данные</param>
        /// <returns>объект</returns>
        public T DeSerializeXmlToObject<T>(XmlDocument inputData) where T : new()
        {
            T xmlObject;
            var xmlSerializer = new XmlSerializer(typeof(T));

            using (var reader = new XmlNodeReader(inputData))
            {
                xmlObject = (T)xmlSerializer.Deserialize(reader);
            }

            return xmlObject;
        }
    }
}