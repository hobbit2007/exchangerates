﻿using System;
using System.IO;

namespace ExchangeRates.Classes
{
    public class Logger
    {
        /// <summary>
        /// Текущий путь расположения
        /// </summary>
        private static readonly string CurPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "log.txt");

        /// <summary>
        /// Добавить запись в лог
        /// </summary>
        /// <param name="errorCode">Код сообщения:-1 - ошибка, 0 - инфмормация, 1 - успешно </param>
        /// <param name="log">Текст сообщения</param>
        public static void SetLog(int errorCode, string log)
        {
            try
            {
                using (var s = new StreamWriter(new FileStream(CurPath, FileMode.Append, FileAccess.Write)))
                {
                    var addit = "";
                    switch (errorCode)
                    {
                        case -1:
                            addit += "<Error>";
                            break;
                        case 1:
                            addit += "<All right>";
                            break;
                        default:
                            addit += "<Info>";
                            break;
                    }
                    s.WriteLine("{0:dd.MM.yyyy HH:mm:ss} {1} : {2};", DateTime.Now, addit, log);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Ошибка при записи данных в log.txt");
            }
        }
    }
}