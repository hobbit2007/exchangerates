﻿using ExchangeRates.Classes;
using ExchangeRates.DataBase;
using System;
using System.Threading;

namespace ExchangeRates
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var dbSetup = new DbInitialize();
                dbSetup.Start(new DataBaseContext());
                var exchangeRate = new ExchangeRate();

                TimerCallback tm = new TimerCallback(exchangeRate.Get);
                Timer timer = new Timer(tm, null, 0, (int)new TimeSpan(1, 0, 0, 0).TotalMilliseconds);

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Logger.SetLog(-1, ex.StackTrace);
            }
        }
    }
}
