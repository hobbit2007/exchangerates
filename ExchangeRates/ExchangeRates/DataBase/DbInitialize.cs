﻿namespace ExchangeRates.DataBase
{
    public class DbInitialize
    {
        public void Start(DataBaseContext context)
        {
            context.Database.CreateIfNotExists();
        }
    }
}