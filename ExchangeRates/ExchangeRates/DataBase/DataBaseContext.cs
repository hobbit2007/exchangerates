﻿using System.Data.Entity;

namespace ExchangeRates.DataBase
{
    public partial class DataBaseContext : DbContext
    {
        public DataBaseContext() : base("name=DataBaseContext") { }

        public virtual DbSet<CurrencyRate> CurrencyRate { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CurrencyRate>()
                .HasIndex(x => new { x.Date, x.Currency }).IsUnique();
        }
    }
}