﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ExchangeRates.DataBase
{
    public class CurrencyRate
    {
        DateTime _date;

        [Key, Column(Order = 0)]
        public DateTime Date { get; set; }

        [Key, Column(Order = 1)]
        public string Currency { get; set; }

        public double Value { get; set; }
    }
}